import multiprocessing
import random
import time

import pandas as pd
import shutil
import os


def factorize_naive(n):
    """ A naive factorization method. Take integer 'n', return list of
        factors.
    """
    if n < 2:
        return []
    factors = []
    p = 2

    while True:
        if n == 1:
            return factors

        r = n % p
        if r == 0:
            factors.append(p)
            n = n / p
        elif p * p >= n:
            factors.append(n)
            return factors
        elif p > 2:
            # Advance in steps of 2 over odd numbers
            p += 2
        else:
            # If p == 2, get to 3
            p += 1

    assert False, "unreachable"


if __name__ == '__main__':
    DATA_SIZE = 1_000_000
    result = {}
    results = {'Workers': [], 'Elapsed time': []}
    for workers in range(6, 22):
        start = time.time()
        with multiprocessing.Pool(workers) as pool:
            input_data = (i for i in range(1, DATA_SIZE + 1))
            result = [
                (n, factors)
                for n, factors in enumerate(pool.map(factorize_naive, input_data), 1)
            ]
        end = time.time() - start
        results['Elapsed time'].append(end)
        results['Workers'].append(workers)
    df = pd.DataFrame(results)
    df.to_excel('process_res.xlsx')
