import os
import time
import random
import string
import requests
from multiprocessing.pool import ThreadPool
import pandas as pd
import shutil


def fetch_pic(num_pic):
    # def fetch_pic(num_pic, path):
    url = 'https://picsum.photos/400/600'
    path = 'pics'
    for _ in range(num_pic):
        random_name = ''.join(random.choices(string.ascii_letters + string.digits, k=5))
        response = requests.get(url)
        if response.status_code == 200:
            with open(f'{path}\{random_name}.jpg', 'wb') as f:
                f.write(response.content)
                # print(f"Fetched pic [{os.getpid()}]: {f.name}")


DATA_SIZE = 100
results = {'Workers': [], 'Elapsed time0': [], 'Elapsed time1': [], 'Elapsed time2': []}

for workers in range(8, 30):
    for i in range(3):
        start = time.time()
        with ThreadPool(workers) as pool:
            input_data = [DATA_SIZE // workers for _ in range(workers)]
            pool.map(fetch_pic, input_data)
        end = time.time() - start
        results[f'Elapsed time{i}'].append(end)
        shutil.rmtree('pics')
        os.mkdir('pics')
    results['Workers'].append(workers)

df = pd.DataFrame(results)
df.to_excel('threads.xlsx')
